// File: ArrayTest.cpp
// Driver for simple class Array
/*=============================================================================
 |   Assignment:  Project 3
 |
 |       Author:  Joshua White
 |     Language: C++, compiled with g++
 |
 |        Class:  CSC 136
 |   Instructor:  Dr.Spiegel
 |     Due Date:  October 31 2014
 |
 +-----------------------------------------------------------------------------
 |
 |  Description: This program is used to test the implementation of the class 	
 |				defined by Array.h
 |

 |
 *===========================================================================*/

#include <iostream>
#include "Array.h"
#include <fstream>

int main()
{
	// create two arrays and print Array count
   Array bigArray1( 20 ), bigArray2, bigArray3, bigArray4(10) ;
   cout << "\nTest File for Array Class\n\n";
	ifstream cfile; 
	cfile.open("p3.txt");
	cfile >> bigArray1;
	cout << endl << endl;
	//Checks the number of elements in biggArray1
	cout << "getNumElts Output for bigArray1: \n";
	cout << bigArray1.getNumElts();
	cout << endl;
	//Checks the capacity in biggArray1
	cout << "getCapacity Output for bigArray1: \n";
	cout << bigArray1.getCapacity();
	cout << endl;
	//Checks what is contained in bigArray1
	cout << "\nItems contained in bigArray1\n";
	cout << bigArray1;//Tests Stream Insertion Operator
	
	bigArray3 = bigArray1;//Test Assignment operator
	cout << "getNumElts Output for bigArray3: \n";
	cout << bigArray3.getNumElts();//Checks the number of elements in biggArray3
	cout << endl;
	//Checks the capacity of in biggArray3
	cout << "getCapacity Output for bigArray3: \n";
	cout << bigArray3.getCapacity();
	cout << endl;
	//Shows that bigArray1 was copied into bigArray3
	cout << "\nItems contained in bigArray3(contains bigArray1's elements): \n";
	cout << bigArray3;

	for (int i = 0; i <= 20; i++){ // array stops before going out of bounds
		bigArray4 += i;
	}
	
	
	//cout << ba4[1] << endl;/*Cause core dump, accessing elements not in use not woking*/
	
	//Check of the == operator to see if arrays are equal, should be output
	if (bigArray1 == bigArray3){
		cout << "\nbigArray1 is = to bigArray3!\n";
	}
//Again check of the == operator to see if arrays are equal, should not output
	if (bigArray1 == bigArray2){
		cout << "\nbigArray1 is = to bigArray2!\n";
	}
	//Checks assignment != bigArray1 == bigArray3 so no output
	if (bigArray1 != bigArray3){
		cout << "\nbigArray1 != bigArray3\n";
	}
	//Checks assignment != bigArray1 != bigArray2 so it should output
	if (bigArray1 != bigArray2){
		cout << "\nbigArray1 != bigArray2\n";
	}
	
	 return 0;
}

