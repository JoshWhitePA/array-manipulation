DebugFlag=-g
cc=/opt/csw/gcc3/bin/g++

test: Array.o testArray.o
	$(cc) -o test Array.o testArray.o $(DebugFlag)

testArray.o: testArray.cpp Array.h
	$(cc) -c testArray.cpp $(DebugFlag)

Array.o: Array.cpp Array.h
	$(cc) -c Array.cpp $(DebugFlag)

clean:
	\rm -f *.o test

