// File: Array.h
// Simple class Array prototype (for integers)
/*=============================================================================
 |   Assignment:  Project 3
 |
 |       Author:  Joshua White
 |     Language: C++, compiled with g++
 |
 |        Class:  CSC 136
 |   Instructor:  Dr.Spiegel
 |     Due Date:  November 12 2014
 |
 *===========================================================================*/

#ifndef ARRAY_H
#define ARRAY_H
#include <fstream>

#include <iostream>

 using namespace std;

class Array {

public:
   Array(int arraySize= 10);              // default constructor
   Array(const Array &init);              // copy constructor
   ~Array();                              // destructor
   
   // Functions updated to use term 'capacity' to better describe use
   void setCapacity(int);		    // set the capacity
   void setNumElts(int);			// sets the number of elements
   int getNumElts()const;			// Returns number of elements
   int getCapacity() const;         // return capacity
   
   
   const Array &operator=( const Array & ); // assign arrays
   bool operator==( const Array & ) const;  // compare equal
   bool operator!=(const Array &right) const;// Determine if two arrays are not equal
   const Array &Array::operator+=( const int & );// Appends array and adds elements 
   
   int &operator[](int);              // l-value subscript operator
   const int &operator[](int) const;  // r-value subscript operator
   static int getArrayCount();          // Return count of arrays instantiated.

private:
   int capacity, 		// capacity of the array
       numElts;  		// Elements in the array in use
   int *ptr;     		// pointer to first element of array
   static int arrayCount;  	// # of Arrays instantiated

};

   ostream &operator<<(ostream &, const Array &); //Overloaded output
   // operator for class Array
  ifstream &operator>>(ifstream &, Array &);//inputs values for partial array.


#endif

