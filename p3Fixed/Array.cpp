// File: Array.cpp
// Member function definitions for class Array
/*=============================================================================
 |   Assignment:  Project 3
 |
 |       Author:  Joshua White
 |     Language: C++, compiled with g++
 |
 |        Class:  CSC 136
 |   Instructor:  Dr.Spiegel
 |     Due Date:  November 12 2014
 |
 +-----------------------------------------------------------------------------
 |
 |  Description: Class to use a partially filled array and alter it.
 |
 *===========================================================================*/



#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <assert.h>
#include <istream>
#include <fstream>
#include "Array.h"
#include <cstdio>

 using namespace std;

// Initialize static data member at file scope
int Array::arrayCount = 0;   // no objects yet


/*------------------------------------------------- Array -----*
         |  Function Array 
         |
         |  Purpose: Default constructor for class Array (default size 10)
         |
         |  Parameters: int arraySize (Import)- Used to allocate custom amount 		
         |				of spaces for array
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
Array::Array(int arraySize)
{
   setCapacity(( arraySize > 0 ? arraySize : 10 )); 
   ptr = new int[getCapacity()]; // create space for array
   assert( ptr != 0 );    // terminate if memory not allocated
   ++arrayCount;          // count one more object
   setNumElts(0);        //

   for ( int i = 0; i < getCapacity(); i++ )
      ptr += 0;          // initialize array 
}


/*------------------------------------------------- Array const -----*
         |  Function Array const
         |
         |  Purpose: Copy constructor for class Array
		 |			 must receive a reference to prevent infinite recursion
         |
         |  Parameters: const Array &init (Import)- copy init into object
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
Array::Array(const Array &init)
{  setCapacity(init.getCapacity());
   ptr = new int[getCapacity()]; // create space for array
   assert( ptr != 0 );    // terminate if memory not allocated
   ++arrayCount;          // count one more object
   setNumElts(0);
   for ( int i = 0; i < getNumElts(); i++ )//
      ptr += init.ptr[i];  // copy init into object  
}


/*------------------------------------------------- ~Array() -----*
         |  Function ~Array()
         |
         |  Purpose: Reclaim space for array and subtract from number of objects
         |			 Destructor for class Array
         |			
         |  Parameters: None
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
Array::~Array()
{
   delete [] ptr;            // reclaim space for array
   --arrayCount;             // one fewer objects
}


/*------------------------------------------------- setCapacity -----*
         |  Function setCapacity
         |
         |  Purpose: Set the Array's size
         |
         |  Parameters: int elts (Import)- assigns capacity to elts
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void Array::setCapacity(int elts)
{ 
	capacity=elts;
}

/*------------------------------------------------- setNumElts -----*
         |  Function setNumElts
         |
         |  Purpose:  Sets the number of elements un use by array
         |
         |  Parameters: int newElts (IN) -- Parameter is used to set number of 	
         |	elements held by numElts
         |
         |  Returns:  void
*-------------------------------------------------------------------*/
void Array::setNumElts(int newElts)
{
 numElts = newElts;//alters numElts
}

/*------------------------------------------------- getNumElts -----*
         |  Function getNumElts
         |
         |  Purpose:  Gets number of elements in use by Array
         |
         |  Parameters: None
         |
         |  Returns:  numElts
*-------------------------------------------------------------------*/
int Array::getNumElts() const 
 {
 	return numElts; 
 }
 
// Get the size of the array
/*------------------------------------------------- getCapacity -----*
         |  Function getCapacity
         |
         |  Purpose:  Gets size of the array
         |
         |  Parameters: None
         |
         |  Returns:  capacity
*-------------------------------------------------------------------*/
int Array::getCapacity() const 
{ return capacity; }


/*------------------------------------------------- operator= -----*
         |  Function operator=
         |
         |  Purpose: For arrays of different sizes, deallocate original
         | 			 left side array, then allocate new left side array.
         |			 const return avoids: ( a1 = a2 ) = a3
         |
         |  Parameters: const Array &right - 
         |
         |  Returns:  *this
*-------------------------------------------------------------------*/
const Array &Array::operator=( const Array &right )
{
 	
   if ( &right != this ) {  // check for self-assignment
      
      if ( getCapacity() != right.getCapacity() ) {
         delete [] ptr;         // reclaim space
         setCapacity(right.getCapacity());     // resize this object
         ptr = new int[getCapacity()]; // create space for array copy
         assert( ptr != 0 );    // terminate if not allocated
      }
      
      setNumElts(right.getNumElts());
      
      for ( int i = 0; i < getNumElts(); i++ ){
         ptr[i] = right[i];  // copy array into object 
         }
   }

   return *this;   // enables x = y = z;
}

/*------------------------------------------------- operator+= -----*
         |  Function: operator+=
         |
         |  Purpose:  To append and add element into array from last element of
         | 			  the array
         |
         |  Parameters: const int &right - Is the number that is appended into 
         |     	 		the array
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
const Array &Array::operator+=( const int &right ) { 
	ptr[getNumElts()] = right;
	setNumElts(getNumElts() +1);
}


/*------------------------------------------------- operator== -----*
         |  Function: operator==
         |
         |  Purpose:  Determine if two arrays are equal and return true, 
         | 			  otherwise return false.
         |
         |  Parameters: const Array &right (Import) - Signifies the array on the 		
         |right side of the  ==
         |
         |  Returns:  Boolean
*-------------------------------------------------------------------*/
bool Array::operator==(const Array &right) const
{
   if ( getCapacity() != right.getCapacity() )
      return false;    // arrays of different sizes
    if (getNumElts() != right.getNumElts()) // 
    	return false;  

   for ( int i = 0; i < getNumElts(); i++ ) //
      if ( ptr[ i ] != right[ i ] )
         return false; // arrays are not equal

   return true;        // arrays are equal
}


/*------------------------------------------------- operator!= -----*
         |  Function: operator!=
         |
         |  Purpose: Determine if two arrays are not equal and
		 |	 		 return true, otherwise return false (uses operator==).
         |
         |  Parameters: const Array &right (Import) - Signifies the array on the right 
         | 				side of the  !=
         |
         |  Returns:  Boolean
*-------------------------------------------------------------------*/
bool Array::operator!=(const Array &right) const  
{  return ! ( *this == right ); }


/*------------------------------------------------- operator[] -----*
         |  Function: operator[]
         |
         |  Purpose: Overloaded subscript operator for non-const Arrays
		 |			 reference return creates an lvalue
         |
         |  Parameters: int subscript (Import) - Used to check if we are 
         | 				accessing elements outside of the in use elements
         |
         |  Returns:  ptr[subscript]
*-------------------------------------------------------------------*/
int &Array::operator[](int subscript)
{
   // check for subscript out of range error
   assert( 0 <= subscript && subscript < getNumElts() && subscript <  getCapacity() );

   return ptr[subscript]; // reference return
}

// Overloaded subscript operator for const Arrays
// const reference return creates an rvalue
/*------------------------------------------------- const operator[] -----*
         |  Function: const operator[]
         |
         |  Purpose: Overloaded subscript operator for const Arrays
		 | 			 const reference return creates an rvalue
         |
         |  Parameters: int subscript (Import) - Used to check if we are 
         | 				accessing elements outside of the in use elements
         |
         |  Returns:  ptr[subscript]
*-------------------------------------------------------------------*/
const int &Array::operator[](int subscript) const
{
   // check for subscript out of range error
   assert( 0 <= subscript && subscript < getNumElts() && subscript <  getCapacity() );
   //stops += from adding past capacity and number of elements because
   // numElts keeps getting incremented.
  
   return ptr[ subscript ]; // const reference return
}


/*------------------------------------------------- getArrayCount -----*
         |  Function: getArrayCount
         |
         |  Purpose:  Return the number of Array objects instantiated
		 |			  static functions cannot be const 
         |
         |  Parameters: None
         |
         |  Returns:  arrayCount
*-------------------------------------------------------------------*/
int Array::getArrayCount() { return arrayCount; }


/*------------------------------------------------- operator>> -----*
         |  Function: operator>>
         |
         |  Purpose: Overloaded input operator for class Array;
		 |			 inputs values for partial array.
         |
         |  Parameters: ifstream &input, Array &a - ifstream is input from file,
         |				a is the array being appended 
         |
         |  Returns:  input
*-------------------------------------------------------------------*/
ifstream &operator>>(ifstream &input, Array &a)
{
  int temp=0;
  int i = 0;
  while ( a.getNumElts() < a.getCapacity() && !input.eof()){ //stops when 
  //elements is at capacity
      	
      input >> temp;//adds file input to temp
      a += temp;//temp variable adds to array 
    temp = 0;//for safety set tem to zero
   }

   return input;   // enables cin >> x >> y;
   
}

/*------------------------------------------------- operator<< -----*
         |  Function: operator<<
         |
         |  Purpose: Overloaded output operator for class Array
		 |			 
         |
         |  Parameters: ostream &output, const Array &a - ostream is what is  	
         |				inserting the stream, a is the object being displayed
         |				a is the array being appended 
         |
         |  Returns:  output
*-------------------------------------------------------------------*/
ostream &operator<<(ostream &output, const Array &a)
{
   int i;
   for ( i = 0; i < a.getNumElts(); i++ ) {
      output << setw( 12 ) << a[ i ];

      if ( ( i + 1 ) % 4 == 0 ) // 4 numbers per row of output
        output << endl;
   }

   if ( i % 4 != 0 )
      output << endl;

   return output;   // enables cout << x << y;
}
