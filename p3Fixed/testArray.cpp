// File: ArrayTest.cpp
// Driver for simple class Array
/*=============================================================================
 |   Assignment:  Project 3
 |
 |       Author:  Joshua White
 |     Language: C++, compiled with g++
 |
 |        Class:  CSC 136
 |   Instructor:  Dr.Spiegel
 |     Due Date:  November 12 2014
 |
 +-----------------------------------------------------------------------------
 |
 |  Description: This program is used to test the implementation of the class 	
 |				defined by Array.h
 |
 |
 *===========================================================================*/

#include <iostream>
#include "Array.h"
#include <fstream>

int main()
{
	// create two arrays and print Array count
   Array bigArray1( 5 ), bigArray2, bigArray3, bigArray4(5), bigArray5(5), bigArray6(2), bigArray7(2), bigArray8(3);
  
  
   
  cout << "\nTest File for Array Class\n\n";
  //test of >>operator
	ifstream cfile; 
	ifstream dfile;
	cfile.open("p3.txt");
	cfile >> bigArray1;//test to see what happens when there are more numbers
	cfile.close();     // in file than spots in array
  	cfile.clear();
	
	//Test of >>Operator to capacity
	dfile.open("p3_2.txt");
	dfile >> bigArray5;// should not coredump even if text file is too large
	//just stops reading if there are not enough spaces
	cout << "Error check of stream extraction operator for bigArray5:\n";
	cout << bigArray5;
	
	
	/*
	//Test of += over capacity, const subscript operator stops 
	//it from going over capacity
	bigArray6 += 1; 
	bigArray6 += 2; 
	bigArray6 += 3;
	
	cout << "\nTest of += over capacity for bigArray6:\n";
	cout << bigArray6;
	*/
	
	cout << endl;
	//Checks the number of elements in biggArray1
	cout << "getNumElts Output for bigArray1: \n";
	cout << bigArray1.getNumElts();
	cout << endl;
	
	//Checks the capacity in biggArray1
	cout << "getCapacity Output for bigArray1: \n";
	cout << bigArray1.getCapacity();
	cout << endl;
	
	//Checks what is contained in bigArray1
	cout << "\nItems contained in bigArray1\n";
	cout << bigArray1;//Tests Stream Insertion Operator
	
	bigArray3 = bigArray1;//Test Assignment operator
	
	cout << "getNumElts Output for bigArray3: \n";
	cout << bigArray3.getNumElts();//Checks the number of elements in biggArray3
	cout << endl;
	
	//Checks the capacity of in biggArray3
	cout << "getCapacity Output for bigArray3: \n";
	cout << bigArray3.getCapacity();
	cout << endl;
	
	//Shows that bigArray1 was copied into bigArray3
	cout << "\nItems contained in bigArray3(contains bigArray1's elements): \n";
	cout << bigArray3;

	
	
	//Check of the == operator to see if arrays are equal, should be output
	if (bigArray1 == bigArray3){
		cout << "\nbigArray1 is = to bigArray3!\n";
	}
  //Again check of the == operator to see if arrays are equal, should not output
	if (bigArray1 == bigArray2){
		cout << "\nbigArray1 is = to bigArray2!\n";
	}
	//Checks assignment != bigArray1 == bigArray3 so no output
	if (bigArray1 != bigArray3){
		cout << "\nbigArray1 != bigArray3\n";
	}
	
	//Checks assignment != bigArray1 != bigArray2 so it should output
	if (bigArray1 != bigArray2){
		cout << "\nbigArray1 != bigArray2\n";
	}
	
	//Test for [ ] Operator
	
  //cout << "\nSubscript test: \n";//test for the subscript operator
  
  /*!enable the first += and cout to test if it allows in use elements*/
  
  // bigArray7 += 1;
  
  // cout << bigArray7[0];

 //*disable first cout and enable += and second cout to test for it actually
 //being out of range
 
 //bigArray7 += 2;
 //cout << bigArray7[2];
 
	 return 0;
}

